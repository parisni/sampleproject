from sample.simple import add_one


class TestCase:
    def test_something(self: object) -> None:
        assert add_one(2) == 3
